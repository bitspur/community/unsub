# unsub

> extract unsubscribe links from email files

python unsub.py path/to/email/files

```

You can export your email files to the `.eml` format
using the [ImportExportTools NG](https://addons.thunderbird.net/en-US/thunderbird/addon/importexporttools-ng)
add-on for [Thunderbird](https://www.thunderbird.net).
```
