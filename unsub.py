from bs4 import BeautifulSoup
from tqdm import tqdm
import csv
import mailbox
import os
import re
import sys

def extract_unsubscribe_links(directory):
    unsubscribe_links = {}
    email_files = [f for f in os.listdir(directory) if f.endswith('.eml')]
    for filename in tqdm(email_files, desc="Scanning emails"):
        with open(os.path.join(directory, filename), 'rb') as file:
            content = file.read()
            try:
                content = content.decode('utf-8', 'ignore')
            except UnicodeDecodeError:
                try:
                    content = content.decode('latin1', 'ignore')
                except UnicodeDecodeError:
                    continue
            msg = mailbox.Message(content)
            from_header = msg.get('From')
            if from_header is None:
                continue
            sender_match = re.search(r'<(.+?)>', from_header)
            sender = (sender_match.group(1) if sender_match else from_header).lower()
            if msg.is_multipart():
                for part in msg.walk():
                    if part.get_content_type() == 'text/html':
                        body = part.get_payload(decode=True)
                        break
            else:
                body = msg.get_payload(decode=True)
            soup = BeautifulSoup(body, 'html.parser')
            unsubscribe_texts = ['unsubscribe', 'opt out', 'optout', 'stop receiving', 'cancel subscription', 'remove me']
            for link in soup.find_all('a', href=True):
                for text in unsubscribe_texts:
                    if text in link.get_text().lower():
                        unsubscribe_link = link['href'].strip()
                        if re.match(r'^[a-zA-Z]*://', unsubscribe_link):
                            to_header = msg.get('To')
                            if to_header is None:
                                continue
                            recipient_match = re.search(r'<(.+?)>', to_header)
                            recipient = (recipient_match.group(1) if recipient_match else to_header).lower()
                            unsubscribe_links[sender] = {
                                'unsubscribe_link': unsubscribe_link,
                                'sender': sender,
                                'recipient': recipient
                            }
    return unsubscribe_links


directory = sys.argv[1]
unsubscribe_links = extract_unsubscribe_links(directory)
filename = 'unsubscribe_links.csv'
if os.path.exists(filename):
    os.remove(filename)
with open(filename, 'w', newline='') as file:
    writer = csv.writer(file)
    for sender, link in unsubscribe_links.items():
        writer.writerow([sender, link['recipient'], link['unsubscribe_link']])
